############ daily stock pickup 1

import requests
from bs4 import BeautifulSoup
import pandas as pd
import threading, time
from multiprocessing import Pool
from fake_useragent import UserAgent
from time import sleep
from multiprocessing import Pool
from concurrent.futures import ThreadPoolExecutor
from bs4 import BeautifulSoup
from selenium import webdriver
import concurrent.futures
import urllib.request
import requests
import time
import sys

global num
global tt_name
global r_data


def name_code(i):
    for page in range(1, 32):
        url = 'https://finance.naver.com/sise/sise_market_sum.nhn?sosok=' + str(i) + '&page=' + str(page)
        r = requests.get(url)
        html = BeautifulSoup(r.text, 'html.parser')
        articles = html.select("tbody td a")
        num = 1
        for obj in articles:
            # print(obj.text)
            if 'main' in obj['href']:
                # print(obj['href'])
                s_list.append([obj.text, obj['href'].split('=')[1]])



def find_stock(url):
    t_code = url[1]
    t_name = url[0]
    df = pd.DataFrame()
    # t_code = df_kosdaq['code'][j]
    url = 'https://finance.naver.com/item/sise_day.nhn?code=' + t_code
    for page in range(1, 13):
        pg_url = '{url}&page={page}'.format(url=url, page=page)
        df = df.append(pd.read_html(pg_url, header=0, encoding='euc-kr')[0], ignore_index=True)
    df = df.dropna()
    s_ratio = 0.1
    n_ratio = 10
    # r_list = [t_name,t_code,float(df['종가'][1]),float(df['종가'][2]),df['거래량'][1],df['거래량'][1:].mean()]
    #print('$$$$$$$$$$',t_name, '$$$$$$$$$$$$$$$')
    if df['종가'][1] > df['종가'][2] * (1 + s_ratio) and df['거래량'][1] > df['거래량'][1:].mean() * n_ratio:
        #r_data.append([t_name, 'https://finance.naver.com/item/main.nhn?code=' + t_code])
        print([t_name, 'https://finance.naver.com/item/main.nhn?code=' + t_code])
    #print('$$$$$$$$$$', t_name, '$$$$$$$$$$$$$$$')



def do_thread_crawl(url: list):

    thread_list = []
    with ThreadPoolExecutor(max_workers=16) as executor:
        thread_list.append(executor.submit(find_stock, url))
        for execution in concurrent.futures.as_completed(thread_list):
            execution.result()


#18 프로세싱 (36 시 full upload intel 9980xe)
if __name__ == "__main__":
    start_time = time.time()
    s_list = []
    name_code(0)
    name_code(1)

    start_time = time.time()
    try:
        while True:
            start_time = time.time()
            with Pool(processes=36) as pool:
                try:
                    pool.map(do_thread_crawl, s_list)
                except:
                    print('bye')
                    print("---%s second ---" % (time.time() - start_time))
                    sys.exit()

            #print("--- elapsed time %s seconds ---" % (time.time() - start_time))
    except KeyboardInterrupt:
        pass

