############ daily stock pickup 1


import requests

from bs4 import BeautifulSoup

import pandas as pd

import threading,time


def name_code(i):

    df_tmp = pd.DataFrame(columns = ['종목명','code'])

    for page in range(1, 32):

        url = 'https://finance.naver.com/sise/sise_market_sum.nhn?sosok=' + str(i) + '&page=' + str(page)

        r = requests.get(url)

        html = BeautifulSoup(r.text, 'html.parser')

        articles = html.select("tbody td a")

        num = 1

        for obj in articles:

            #print(obj.text)

            if 'main' in obj['href']:

            #print(obj['href'])

                df_tmp = df_tmp.append({'종목명' : obj.text,'code' : obj['href'].split('=')[1]}, ignore_index=True)

    return df_tmp


df_kospi = name_code(0)            

df_kosdaq = name_code(1)


############ daily stock pickup 2

import pandas as pd

import threading,time

### setting percentage



#df=pd.DataFrame(columns = ['맥아이씨에스종가'])

def find_stock(t_name,t_code):

    df=pd.DataFrame()

    #t_code = df_kosdaq['code'][j]

    url = 'https://finance.naver.com/item/sise_day.nhn?code=' + t_code

    for page in range(1,13):

        pg_url = '{url}&page={page}'.format(url=url, page=page)

        df = df.append(pd.read_html(pg_url, header=0,encoding='euc-kr')[0], ignore_index=True)

    t_num = []

    for i in range(len(df)):

        if type(df['날짜'][i]) == str:

        #print(type(df['날짜'][i]))

        #print(df['거래량'][i])

            t_num.append(df['거래량'][i])

    #if df['거래량'][1] > df['거래량'].mean()*10 and float(df['종가'][1]) > float(df['종가'][2])*1.1:

    #    return true


    

    r_list = [t_name,t_code,float(df['종가'][1]),float(df['종가'][2]),int(t_num[0]),int(sum(t_num[1:])/len(t_num[1:]))]

    return r_list

    '''

    if t_num[0] > sum(t_num[1:])*5/len(t_num[1:]) and float(df['종가'][1]) > float(df['종가'][2])*1.05:

        print(int(t_num[0]), int(sum(t_num[1:])/len(t_num[1:])),sep = '  /거래량/  ')

        print(float(df['종가'][1]),float(df['종가'][2]),sep = '  /종가/  ')        

        return []

    else:

        return []

    '''

n_list = ['종목명','code','종가','전날종가','거래량','거래량평균']

df_day = pd.DataFrame(columns = n_list)

for i in range(len(df_kosdaq)):

#for i in range(3):

    #print(df_kosdaq['종목명'][i])

    r_dates = find_stock(df_kosdaq['종목명'][i],df_kosdaq['code'][i])

    df_day.loc[i] = r_dates

    #df_day = df_day.append({'종목명' : df_kosdaq['종목명'][i],'code' : df_kosdaq['code'][i]}, ignore_index=True)

    #for j in range(len(r_dates)):

        #df_day = df_day.append({n_list[j] : r_dates[j]}, ignore_index=True)

        #df_day.loc[i] = r_dates

        

        

#find_stock(df_kosdaq['code'][0])


############ daily stock pickup 4 코스피


df_day_kospi = pd.DataFrame(columns = n_list)

for i in range(len(df_kospi)):

#for i in range(3):

    #print(df_kosdaq['종목명'][i])

    r_dates = find_stock(df_kospi['종목명'][i],df_kospi['code'][i])

    df_day_kospi.loc[i] = r_dates

    

############ daily stock pickup 3 코스닥

s_ratio = 0.1

n_ratio = 10

for i in range(len(df_day)):

    if df_day['종가'][i] > df_day['전날종가'][i]*(1+s_ratio) and df_day['거래량'][i] > df_day['거래량평균'][i]*n_ratio:

        print(df_day['종목명'][i], 'https://finance.naver.com/item/main.nhn?code=' + df_day['code'][i] )

        

############ daily stock pickup 5 코스피

s_ratio = 0.1

n_ratio = 10

for i in range(len(df_day_kospi)):

    if df_day_kospi['종가'][i] > df_day_kospi['전날종가'][i]*(1+s_ratio) and df_day_kospi['거래량'][i] > df_day_kospi['거래량평균'][i]*n_ratio:

        print(df_day_kospi['종목명'][i], 'https://finance.naver.com/item/main.nhn?code=' + df_day_kospi['code'][i] )

        
